def call(String repoUrl) {
  pipeline {
       agent any
       stages {
           stage("Tools initialization") {
               steps {
                   sh "mvn --version"
                   sh "java -version"
               }
           }
           stage("Checkout Code") {
               steps {
                   git branch: 'main',
                       url: "${repoUrl}"
               }
           }
           stage("Build") {
               agent { 
            label 'master'
        }
               steps {
                   script{
                try{
                   sh "mvn clean install"
                }
                catch(e){
		    currentBuild.result="Failure"
		   office365ConnectorSend message: "Check Logs | username : log | password : P@ssw0rd", status: "Failed", webhookUrl: "${env.Notification}"
		    throw(e)
		}
               }
               }
           }
           stage("backup"){
             agent { 
            label 'sata'
        }
        steps{
            script{
        sh "mv ${env.sitpath}/${env.JOB_NAME}/*.jar ${env.sitpath}/${env.JOB_NAME}/backup/"
    	}
       }
     }
    stage('copy jar'){
        agent { 
            label 'master'
        }
            steps{
                script{
                     sh "scp -i ${env.pempath} ${env.WORKSPACE}/target/*.jar ${env.userip}:${env.sitpath}/${env.JOB_NAME}/"
                }
            }
    }
    stage("Remove sources"){
        agent { 
            label 'master'
        }
           steps{
               sh "rm -rf /${env.WORKSPACE}/${env.JOB_NAME}/*"
           }
       }
       stage ("JaCoCo") {
 steps {
 junit "*/rezlive_test/*.xml"
 step([$class:"jacocoPublisher" ])
 }
 }
 stage('Code Analysis'){
     steps{
        sh "echo ${env.WORKSPACE}"
     }
     steps{
        script{
            def scannerHome = tool 'SonarQube';
               withSonarQubeEnv("SonarQube") {
               sh "${scannerHome}/bin/sonar-scanner \
               -Dsonar.projectKey=sonartest \
                -Dsonar.host.url=http://35.154.250.28:6060 \
                -Dsonar.login=3734745bde11f9108d1ba2b34c7c5dd762842b81\
               -Dsonar.projectBaseDir=/${env.WORKSPACE}\
                -Dsonar.java.binaries=/${env.WORKSPACE}/target/classes"
            }
        }
     }
 }
       stage('kill service'){
         agent { 
            label 'sata'
        }
        steps{
            script{
                 sh "cd ${env.sitpath}${env.JOB_NAME} ${env.kill}"
                  }
        }
    }
       stage('deployment'){
           agent { 
            label 'sata'
        }
            steps{
                script{
                    sh "cd ${env.sitpath}${env.JOB_NAME}; ${env.jdkpath} -jar ${env.profilepath}"
                }
            }
       }
   }
}
}
